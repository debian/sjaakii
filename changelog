sjaakii (1.4.1-3) unstable; urgency=medium

  * Team upload (Debian namespace).
  * Remove dh-buildinfo usage. (Closes: #1089905)

 -- Chris Hofstaedtler <zeha@debian.org>  Sat, 14 Dec 2024 14:41:45 +0100

sjaakii (1.4.1-2) unstable; urgency=medium

  * Switch Vcs-* to salsa.
  * Bump Standards-Version to 4.5.0, no change.

 -- Yann Dirson <dirson@debian.org>  Fri, 03 Apr 2020 11:48:20 +0200

sjaakii (1.4.1-1) unstable; urgency=medium

  * New upstream release.

 -- Yann Dirson <dirson@debian.org>  Sun, 06 Aug 2017 20:33:44 +0200

sjaakii (1.3.1a-1) unstable; urgency=medium

  * New upstream release.
  * Migrate to dbgsym.

 -- Yann Dirson <dirson@debian.org>  Sun, 10 Jul 2016 23:56:16 +0200

sjaakii (1.3.0-1) unstable; urgency=medium

  * New upstream release.

 -- Yann Dirson <dirson@debian.org>  Tue, 29 Mar 2016 22:51:22 +0200

sjaakii (1.2.1-1) unstable; urgency=medium

  * New upstream release.
    * Removed patch fix-version, now useless.

 -- Yann Dirson <dirson@debian.org>  Mon, 14 Mar 2016 23:40:38 +0100

sjaakii (1.0.0-1) unstable; urgency=medium

  * Final upstream release.
  * Fixed CMakeLists so the version advertises as 1.0.0, not 1.0rc7.

 -- Yann Dirson <dirson@debian.org>  Mon, 23 Mar 2015 23:27:00 +0100

sjaakii (1.0rc5-1) unstable; urgency=medium

  * New upstream pre-release - the previous upload was not final 1.0
    after all (final release was posponed but the tarball stayed around).
  * Fix debian/watch to look at the official download page instead of the
    download dir, to avoid unwanted snapshot and other problems.

 -- Yann Dirson <dirson@debian.org>  Wed, 25 Feb 2015 23:21:00 +0100

sjaakii (1.0-1) unstable; urgency=medium

  * Final upstream release.
    * stopped using __DATE__, dropping our patch.
  * Changed debian/watch to only include versions starting with a digit or
    the "beta" string (eg. excluding those starting with "svn").

 -- Yann Dirson <dirson@debian.org>  Mon, 16 Feb 2015 18:36:01 +0100

sjaakii (1.0~rc2b-1) unstable; urgency=medium

  * New upstream pre-release.
  * Remove use of __DATE__ for reproducibility.
  * Emphasize the "many" supported variants in short description.

 -- Yann Dirson <dirson@debian.org>  Tue, 03 Feb 2015 22:43:45 +0100

sjaakii (1.0~rc1-1) unstable; urgency=medium

  * New upstream pre-release.
  * Added menu hints for running in XBoard with Shogi and XiangQi themes.
  * Updated debian/watch for rc versions

 -- Yann Dirson <dirson@debian.org>  Mon, 02 Feb 2015 20:48:12 +0100

sjaakii (0~beta5b+r180-1) unstable; urgency=medium

  * New upstream release
  * Fixed Vcs-Browser URL.
  * Add copyright paragraphs for files that were originally GPL-2 and
    public-domain (noticed by Thorsten Alteholz).

 -- Yann Dirson <dirson@debian.org>  Fri, 23 Jan 2015 22:29:08 +0100

sjaakii (0~beta5b-1) unstable; urgency=medium

  * New upstream beta release.

 -- Yann Dirson <dirson@debian.org>  Sat, 06 Dec 2014 18:05:15 +0100

sjaakii (0~beta5-1) unstable; urgency=medium

  * New upstream beta release.

 -- Yann Dirson <dirson@debian.org>  Wed, 03 Dec 2014 20:49:38 +0100

sjaakii (0~beta4b-1) unstable; urgency=medium

  * New upstream upload for beta4 (replaced tarball) to fix crash with
    variants defined in variants.txt.

 -- Yann Dirson <dirson@debian.org>  Mon, 01 Dec 2014 23:23:05 +0100

sjaakii (0~beta4-1) unstable; urgency=medium

  * New upstream beta release.
  * Added libreadline-dev as build-dep.
  * Use dh_install --fail-missing

 -- Yann Dirson <dirson@debian.org>  Sat, 29 Nov 2014 11:05:19 +0100

sjaakii (0~beta3-1) unstable; urgency=low

  * Initial release (Closes: #770384).
  * Deactivated SSE42 instruction set to get a portable binary.

 -- Yann Dirson <dirson@debian.org>  Sun, 23 Nov 2014 16:57:59 +0100
